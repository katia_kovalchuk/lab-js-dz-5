import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

const PUBLIC_COMPONENTS: any[] = [];
const PUBLIC_DIRECTIVES: string[] = [];
const PUBLIC_PIPES: string[] = [];

@NgModule({
  declarations: [
    ...PUBLIC_COMPONENTS,
    ...PUBLIC_DIRECTIVES,
    ...PUBLIC_PIPES,
  ],
  imports: [
    CommonModule
  ],
  exports: [
    CommonModule,
    ...PUBLIC_COMPONENTS,
    ...PUBLIC_DIRECTIVES,
    ...PUBLIC_PIPES,
  ],
})
export class SharedModule { }
